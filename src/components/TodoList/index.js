import { Col, Row, Input, Button, Select, Tag } from 'antd';
import Todo from '../Todo';
import { useDispatch, useSelector } from 'react-redux';
import { addTodo } from '../../redux/actions';
import { v4 as uuidv4 } from "uuid"
import { useState } from 'react';
import { todosRemainingSelector } from '../../redux/selectors';

export default function TodoList() {
    const dispatch = useDispatch();

    const [todoName, setTodoName] = useState("");
    const [priority, setPriority] = useState('Medium');

    const todoList = useSelector(todosRemainingSelector);

    const handlerTodoName = (e) => {
        setTodoName(e.target.value);
    }

    const handlerPriority = (value) => {
        setPriority(value);
    }

    const handlerAddButtonClick = () => {
        dispatch(addTodo({
            id: uuidv4(),
            completed: false,
            name: todoName,
            priority: priority,
        }))

        setTodoName('');
    }

  return (
    <Row style={{ height: 'calc(100% - 40px)' }}>
      <Col span={24} style={{ height: 'calc(100% - 40px)', overflowY: 'auto' }}>
        {todoList.map(todo => <Todo key={todo.id} id={todo.id} name={todo.name} prioriry={todo.priority} completed={todo.completed}/>)}
      </Col>
      <Col span={24}>
        <Input.Group style={{ display: 'flex' }} compact>
          <Input value={todoName} onChange={e => handlerTodoName(e)}/>
          <Select defaultValue="Medium" onChange={handlerPriority}>
            <Select.Option value='High' label='High'>
              <Tag color='red'>High</Tag>
            </Select.Option>
            <Select.Option value='Medium' label='Medium'>
              <Tag color='blue'>Medium</Tag>
            </Select.Option>
            <Select.Option value='Low' label='Low'>
              <Tag color='gray'>Low</Tag>
            </Select.Option>
          </Select>
          <Button type='primary' onClick={handlerAddButtonClick}>
            Add
          </Button>
        </Input.Group>
      </Col>
    </Row>
  );
}