const initialState = [
        { id: 1, name: "JS", completed: false, priority: "Medium"},
        { id: 2, name: "React", completed: true, priority: "High"},
        { id: 3, name: "Java", completed: false, priority: "Low"}


    ]

const todoReducer = (state = initialState, action) => {
    switch(action.type) {
        case 'todoList/addTodo':
            return [
                ...state,
                action.payload
            ]
        case 'todoList/toggleTodoStatus':
            state.filter(todo => {
                if(todo.id === action.payload) {
                    todo.completed = !todo.completed;
                }
            }) 
        default:
            return state;
    }
}

export default todoReducer