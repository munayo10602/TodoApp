const initialState = {
        search: '',
        status: 'All',
        priority: []
    }

const filterReducer = (state = initialState, action) => {
    switch(action.type) {
        case 'totoList/searchFilterChange':
            return {
                ...state,
                search: action.payload,
            }
        case 'todoList/statusFilterChange':
            return {
                ...state,
                status: action.payload
            }
        case 'todoList/prioritiesFilterChange':
            return {
                ...state,
                priority: action.payload
            }
        default: 
            return state;
    }
}

export default filterReducer