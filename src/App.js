import { Typography, Divider } from 'antd';
import TodoList from './components/TodoList';
import Filters from './components/Filters';

const { Title } = Typography;

function App() {
  return (
    <div
      style={{
        width: 500,
        margin: '0 auto',
        display: 'flex',
        flexDirection: 'column',
        backgroundColor: 'white',
        padding: 20,
        boxShadow: '0 0 10px 4px #bfbfbf',
        borderRadius: 5,
        height: '90vh',
      }}
    >
      <Title style={{ textAlign: 'center' }}>TODO APP with REDUX</Title>
      <Filters />
      <Divider />
      <TodoList />
    </div>
  );
}

export default App;


// import {configureStore, createReducer, createAction, createSlice } from "@reduxjs/toolkit"

// const increment = createAction("INCREMENT");
// const decrement = createAction("DECREMENT");

// function App() {
//   const slideReducer = createSlice({
//     name: "todo",
//     initialState: {count: 0},
//     reducers: {
//       addTodo: (state, action) => {
        
//       }
//     }
//   })
//   const initialState = {
//     count: 0,
//   }

//   const handlerReducer = createReducer(initialState, (builder) => {
//     builder
//     .addCase("INCREMENT", (state) => { state.count+=1})
//     .addCase("DECREMENT", (state) => { state.count-=1})
//   })

//   const store = configureStore({
//     reducer: handlerReducer,
//   })

//   store.dispatch(increment());
//   store.dispatch(decrement());
//   store.dispatch(decrement());

//   console.log(store.getState());

//   return (
//     <div className="App">
//       <ul id="hobbyListId">
//       </ul>
//     </div>
//   );
// }

// export default App;
