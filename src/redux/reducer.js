import { combineReducers } from "redux"
import filterReducer from "../components/Filters/FiltersSlice"
import todoReducer from "../components/TodoList/TodosSlice"

export const rootReducer = combineReducers({
    filters: filterReducer,
    todoList: todoReducer
})