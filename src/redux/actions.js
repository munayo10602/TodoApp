export const addTodo = (data) => {
    return {
        type: 'todoList/addTodo',
        payload: data
    }
}

export const toggleTodoStatusChange = (id) => {
    return {
        type: 'todoList/toggleTodoStatus',
        payload: id
    }
}

export const searchFilterChange = (text) => {
    return {
        type: 'totoList/searchFilterChange',
        payload: text
    }
}

export const statusFilterChange = (status) => {
    return {
        type: 'todoList/statusFilterChange',
        payload: status
    }
}

export const prioritiesFilterChange = (status) => {
    return {
        type: 'todoList/prioritiesFilterChange',
        payload: status
    }
}