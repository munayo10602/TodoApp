import { createSelector } from "@reduxjs/toolkit";

const todoListSelector = state => state.todoList;

const searchTextSelector = state => state.filters.search;

const filterStatusSelector = state => state.filters.status;

const filterPrioritiesSelector = state => state.filters.priority;



export const todosRemainingSelector = createSelector(todoListSelector, searchTextSelector, filterStatusSelector, filterPrioritiesSelector, (todoList, searchText, status, priority) => {
        return todoList.filter(todo => {
            if(priority.length === 0) {
                if(status === 'All') {
                    return todo.name.includes(searchText)
                }
                return (todo.name.includes(searchText) && (status === 'Completed' ? todo.completed : !todo.completed))
            }else {
                if(status === 'All') {
                    return (todo.name.includes(searchText) && priority.includes(todo.priority))
                }
                return (todo.name.includes(searchText) && (status === 'Completed' ? todo.completed : !todo.completed) && priority.includes(todo.priority))
            }

        })
})